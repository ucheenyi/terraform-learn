provider "aws" {
    region = "eu-west-2"

}

resource "aws_vpc" "uc-vpc" {
    cidr_block = "10.0.0.0/16"  
    tags = {
       Name: "uc-vpc"
       
   }
}



resource "aws_subnet" "uc-subnet"{
    vpc_id = aws_vpc.uc-vpc.id
    cidr_block = var.subnet_1_cidr_block
    availability_zone = "eu-west-2a"
    tags = {
        Name: "uc-subnet-1"
    }
}

data "aws_vpc" "existing-vpc" {
    default = true
}

resource "aws_subnet" "uc-subnet-2" {
    vpc_id = data.aws_vpc.existing-vpc.id
    cidr_block = "172.31.48.0/20"
    availability_zone = "eu-west-2a"
       tags = {
       Name: "uc-subnet-default"
   }
}

output "vpc_out" {
    value = aws_vpc.uc-vpc.id
}

output "subnet-id" {
    value = aws_subnet.uc-subnet.id
}

variable "subnet_1_cidr_block" {
  type        = string
  default     = ""
  description = "cidr block for the subnet-1"
}
